INTRODUCTION
------------

Simple module that provides integration with libreoffice binary.
Generates nodes from the given DOC(x) documents and stores 
generated HTML as a content for target textarea field.
Optionally you can store the doc(x) document in the target Content type file field.

REQUIREMENTS
------------

Requires libreoffice package to be available to the CLI.
Make sure soffice command available to your CLI.

INSTALLATION 
------------

Install libreoffice binary.

CONFIGURATION
------------

1. Enter doc(x) files folder location.
2. Choose Target Content Type
3. Choose target field where to store generated HTML
4. Optionally set file field to store processed doc(x) file.
